var express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server),
    sha1 = require('sha1'),
    readline = require("readline");

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var auth_key = "uV1lf2[1!6J6Zj4Vt7,bruw?+5`^d8";
var server_port = 2222;

try
{
    server.listen(server_port);
    var identifiedConnections = 0;
    var totalConnections = 0;

    io.on('connection', function (socket) {
        totalConnections++;

        var user_id = null;
        var name = null;
        var profile = null;

        socket.on("identify", function(data) {
            if (sha1("user-"+data.user_id+'-'+data.name+'-'+auth_key) == data.auth)
            {
                user_id = data.user_id;
                name = data.name;
                profile = data.profile;
                identifiedConnections++;
                socket.emit("user authenticated", name);
                socket.join("global msg");
            }
            else
            {
                console.log("user trying to authenticate", data);
            }
        });

        socket.on("message send", function (data) {
            if (user_id != null)
            {
                console.log("sending message", data.msg);
                socket.broadcast.in("global msg").emit("message recv", {
                    user_id: user_id,
                    name: name,
                    msg: data.msg,
                    profile: profile
                });
            }
        });

        socket.on("disconnect", function(){
            if (user_id != null)
            {
                console.log("user disconnected: ", name);
                identifiedConnections--;
            }
            totalConnections--;
        });
    });

    console.log("server is running on port", server_port);

    rl.on("line", function (input){
        if (input == "watch")
        {
            console.log("Identified connections: ", identifiedConnections);
            console.log("Unidentified connections: ", totalConnections - identifiedConnections);
            console.log("Total connections: ", totalConnections);
        }
    });
}catch(err)
{
    console.log(err.message);
}
