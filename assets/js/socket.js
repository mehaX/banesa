var socket = null;
var connected = false;

var node_connect = function()
{
    socket = io.connect(node_url, {secure: node_secure});
    socket.on("connect", function () {
        console.log("connected");
        connected = true;

        socket.emit("identify", {
            name: personi,
            user_id: user_id,
            profile: profile,
            auth: auth
        });
    });

    socket.on("user authenticated", function(data){
        console.log("user authenticated");
    });

    socket.on("message recv", function(data){
        var last_message = $('#msg_popup .panel-body .msg').last();
        if (last_message.data('user_id') == data.user_id)
        {
            last_message.find('.messages ul').append('<li><p>'+data.msg+'</p></li>');
        }
        else if (user_id == data.user_id)
        {
            $('#msg_popup .panel-body').append(
                '<div class="row msg msg-right" data-user_id="'+data.user_id+'">' +
                    '<div class="col-xs-10 messages">' +
                        '<ul>' +
                            '<li>' +
                                '<p>'+data.msg+'</p>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<div class="col-xs-2 profile">' +
                        '<img src="'+path+'uploads/users/thumbnails/'+data.profile+'">' +
                    '</div>' +
                '</div>');
        }
        else
        {
            $('#msg_popup .panel-body').append(
                '<div class="row msg msg-left" data-user_id="'+data.user_id+'">' +
                    '<div class="col-xs-2 profile">' +
                        '<img src="'+path+'uploads/users/thumbnails/'+data.profile+'">' +
                    '</div>' +
                    '<div class="col-xs-10 messages">' +
                        '<ul>' +
                            '<li>' +
                                '<p>'+data.msg+'</p>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                '</div>');
        }
        var panel_body = $('#msg_popup .panel-body');
        panel_body.animate({scrollTop: panel_body.prop("scrollHeight")}, "slow");
    });

    socket.on("disconnect", function () {
        connected = false;
        console.log("disconnected");
        //node_connect();
    });
};

var retryConnectOnFailure = function(retryInMilliseconds) {
    setTimeout(function() {
        if (!connected) {
            node_connect();
            retryConnectOnFailure(retryInMilliseconds);
        }
    }, retryInMilliseconds);
};
node_connect();

