/**
 * Created by mehaX on 6/25/2016.
 */

function post_new_message()
{
    var msg = $('.post-message-js input').val();
    if (msg == "")
        return;

    var msg_filter = msg;
    msg_filter = msg_filter.replace(/</g, "&lt;");
    msg_filter = msg_filter.replace(/>/g, "&gt;");
    msg_filter = msg_filter.replace(/&/g, "&amp;");
    msg_filter = msg_filter.replace(/\n/g, "<br>");

    $('.post-message-js input').val('');
    var last_message = $('#msg_popup .panel-body .msg').last();
    if (last_message.hasClass('msg-right'))
    {
        last_message.find('.messages ul').append('<li><p>'+msg_filter+'</p></li>');
    }
    else
    {
        $('#msg_popup .panel-body').append(
            '<div class="row msg msg-right" data-user_id="'+user_id+'">' +
                '<div class="col-xs-10 messages">' +
                    '<ul>' +
                        '<li>' +
                            '<p>'+msg_filter+'</p>' +
                        '</li>' +
                    '</ul>' +
                '</div>' +
                '<div class="col-xs-2 profile">' +
                    '<img src="'+path+'uploads/users/thumbnails/'+profile+'">' +
                '</div>' +
            '</div>');
    }
    var panel_body = $('#msg_popup .panel-body');
    panel_body.animate({scrollTop: panel_body.prop("scrollHeight")}, "slow");

    $.ajax({
        type: 'post',
        url: path+'chat/post_new_message',
        data: {msg: msg},
        dataType: 'json',
        cache: false,
        success: function(res){
            socket.emit("message send", {msg: res.msg});
        }
    });
}

$(document)

.on('click', '.shto-produkt-js', function(){
    var last = $('.produktet-js tr:last');
    var row_id = $.now();
    $('.produktet-js tbody').append('<tr class="produkti-row-js">' + last.html() + '</tr>');

    last = $('.produktet-js tr:last');
    last.find('input[type=text]').each(function(){$(this).val('');});
    var mujore = $('.mujore-js').data('mujore');
    if (mujore == true)
        last.find('.btn-checkbox').each(function () { //if (!$(this).hasClass('active'))
            $(this).addClass('active');
        });
    else
        last.find('.btn-checkbox.active').each(function () {
            $(this).removeClass('active');
        });
    last.find(".dropup input[type='checkbox']").each(function(){
        $(this).prop('checked', mujore == true);
    });
    var i = 0;
    last.find('.funkyradio > div').each(function(){
        var id = row_id + '-' + i++;
        $(this).find('input').attr('id', id);
        $(this).find('label').attr('for', id);
    });
    last.find('.sasia-js').val('1');
    last.find('.fshij-produkt-js.hidden, .dropdown-menu li.hidden').each(function(){$(this).removeClass('hidden');});
})

.on('click', '.fshij-produkt-js', function(){
    $(this).closest('tr').remove();
})

.on('click', '.blerja-submit-js', function(){
    var personi_id = $('.blerja-personi-js').val();
    var produktet = [];

    $('.produkti-row-js').each(function(){
        var produkti = {
            emri: $(this).find('.emri-js').val(),
            qmimi: $(this).find('.qmimi-js').val().replace(',', '.'),
            borxhet: []
        };
        var sasia = $(this).find('.sasia-js').val();
        if (typeof sasia != 'undefined')
            produkti['sasia'] = sasia.replace(',', '.');
        if (produkti.emri == '' || produkti.qmimi == '')
            return;

        $(this).find('.borxhet-js .btn-checkbox').each(function(){
            if ($(this).hasClass('active'))
                produkti.borxhet.push($(this).data('personi'));
        });

        produktet.push(produkti);
    });

    var data = {
        personi_id: personi_id,
        produktet: produktet,
        lista: $('.lista-js').val(),
        muaji: $('.muaji-js').val(),
        viti: $('.viti-js').val()
    };

    $.ajax({
        type: 'post',
        data: data,
        dataType: 'json',
        cache: false,
        success: function(html){
            window.location = html.path;
        }
    });
})

.on('click', '.blerja-toggle-js', function(){
    if (!confirm('A jeni te sigurte?'))
        return;

    var body = $(this).closest('.blerja-row');
    var blerja_id = body.data('id');
    var status = body.data('status');
    body.fadeOut(400, function(){
        body.remove();
    });

    $.ajax({
        type: 'get',
        url: path + (status == 0 ? 'blerja-paguaj/' : 'blerja-aktivizo/') + blerja_id,
        cache: false
    });
})

.on('click', '.blerja-row .media', function(){
    var panel = $(this).closest('.blerja-row');
    var button = $(this).find('.llogarit-blerjen-js span');
    var applied = panel.data('applied') == 'true';

    var infos = panel.find('.user-infos');
    infos.slideToggle(applied ? 400 : 100, function(){
        if (infos.is(':visible'))
        {
            button.removeClass('glyphicon-chevron-down');
            button.addClass('glyphicon-chevron-up');
        }
        else
        {
            button.removeClass('glyphicon-chevron-up');
            button.addClass('glyphicon-chevron-down');
        }

        if (applied)
            return;

        var body = infos.find('.blerja-body-js');
        var blerja_id = panel.data('id');
        var data = {};

        var url = null;
        if (!blerja_id)
        {
            data.month = panel.data('month');
            data.year = panel.data('year');
            url = 'mujore/llogarit';
        }
        else
            url = 'blerja-llogarit/' + blerja_id;

        $.ajax({
            type: 'post',
            url: path + url,
            dataType: 'json',
            data: data,
            cache: false,
            success: function(html){
                body.html(html.content);

                body.slideDown();

                panel.data('applied', 'true');
            }
        });
    });


})

.on('click', '.borxhi-toggle-js', function(){
    var li          = $(this).closest('.list-group-item');
    var checkbox    = $(this);
    var paguaj      = (($(this).data('paguar') + 1)%2 == 1);
    var blerja_id   = $(this).closest('.blerja-row').data('id');
    var personi_id  = li.data('personi');

    if (paguaj == false)
    {
        li.removeClass('list-group-item-success');
        checkbox.removeClass('glyphicon-check');
        checkbox.addClass('glyphicon-unchecked');
    }
    else
    {
        li.addClass('list-group-item-success');
        checkbox.removeClass('glyphicon-unchecked');
        checkbox.addClass('glyphicon-check');
    }
    $(this).data('paguar', paguaj ? 1 : 0);

    $.ajax({
        type: 'post',
        url: path + 'borxhi-paguaj',
        data: {blerja_id: blerja_id, personi_id: personi_id, paguaj: paguaj},
        cache: false,
        success: function(html){
            li.closest('.panel').find('.totali-val-js').html(html);
        }
    });
})

.on('click', '.borxhi-paguaj-js', function(){
    var modal = $('#myModal');
    modal.modal('show');

    var tr          = $(this).closest('tr');
    var bleresi_id  = tr.data('bleresi');
    var personi_id  = tr.data('personi');
    var lista_id    = tr.closest('.panel').data('lista');

    var paguar      = tr.find('.paguar .value').text();

    modal.data('bleresi', bleresi_id);
    modal.data('personi', personi_id);
    modal.data('lista', lista_id);

    modal.find('.onoffswitch4').removeClass('no');

    modal.find('#myonoffswitch4').prop('checked', paguar == "");
    modal.find('.onoffswitch4').toggleClass('no', paguar != "");
    modal.find('.modal-shuma-js .shuma').val(paguar);

    if (paguar == "")
        modal.find('.modal-shuma-js').hide();
    else
        modal.find('.modal-shuma-js').show();
})

.on('click', '.borxhi-paguaj-update-js', function(){
    var modal = $('#myModal');

    var data = {
        bleresi_id: modal.data('bleresi'),
        personi_id: modal.data('personi'),
        lista_id: modal.data('lista'),
        total: modal.find('#myonoffswitch4').is(':checked')
    };

    var tr = $('.panel[data-lista="'+data.lista_id+'"] tr[data-personi="'+data.personi_id+'"]'+(data.lista_id != 0 ? '[data-bleresi="'+data.bleresi_id+'"]' : ""));

    if (data.total)
        tr.remove();
    else
    {
        data.shuma = modal.find('.modal-shuma-js .shuma').val().replace(',', '.');

        tr.find('.paguar .value').text(data.shuma);
        if (data.shuma != "")
            tr.find('.paguar').show();
        else
            tr.find('.paguar').hide();
    }

    modal.modal('hide');
    $.ajax({
        type: 'post',
        url: path + 'paguaj-totalin',
        data: data,
        cache: false,
        success: function()
        {
            location.reload();
        }
    })
})

.on('click', '#myonoffswitch4', function(){
    $('#myModal .onoffswitch4').toggleClass('no');
    $('#myModal .modal-shuma-js').fadeToggle();
    $('#myModal .modal-shuma-js .shuma').val();
})

// nga xs
.on('click', '.produkti-row-js .dropdown-js input', function(){
    var personi = $(this).data('personi');
    var checked = $(this).is(':checked');

    var input = $(this).closest('.produkti-row-js').find('.btn-checkbox[data-personi="'+personi+'"]');

    if (checked)
        input.addClass('active');
    else
        input.removeClass('active');
})
// ne xs
.on('click', '.produkti-row-js .btn-checkbox', function(){
    var personi = $(this).data('personi');
    var checked = !$(this).hasClass('active');

    var input = $(this).closest('.produkti-row-js').find('.dropdown-js input[data-personi="'+personi+'"]');

    input.prop('checked', checked);
})

.on('click', '.produkti-row-js .dropdown-menu', function (e) {
    e.stopPropagation();
})

.on('click', '.number-spinner button', function () {
    var btn = $(this),
        oldValue = btn.closest('.number-spinner').find('input').val().trim(),
        newVal = 0;

    if (btn.attr('data-dir') == 'up') {
        newVal = parseInt(oldValue) + 1;
    } else {
        if (oldValue > 1) {
            newVal = parseInt(oldValue) - 1;
        } else {
            newVal = 1;
        }
    }
    btn.closest('.number-spinner').find('input').val(newVal);
})

.on('click', '#msg_popup.msg-collapsed, #msg_popup.msg-expanded .panel-title', function(){
    $(this).closest('.panel').toggleClass('msg-expanded');
    $(this).closest('.panel').toggleClass('msg-collapsed');
    $('body').toggleClass('disable-scroll');

    if ($(this).closest('.panel').hasClass('msg-expanded'))
        $(this).closest('.panel').find('.panel-footer input').focus();
})

.on('click', '.post-message-js button', post_new_message)

.on('keyup', '.post-message-js input', function(e){
    if (e.keyCode == 13)
    {
        post_new_message();
    }
})

.on('click', '.personale-js', function(){
    var personi_id = $(this).data('id');
    var modal = $('#personaleModal');

    modal.find('.modal-title').text($(this).text());
    modal.find('.modal-body').html('');
    modal.modal('show');

    $.ajax({
        type: 'get',
        url: path + 'borxhi/personale_klienti/'+personi_id,
        cache: false,
        success: function(html){
            modal.find('.modal-body').html(html);
        }
    });
})

.on('click', '.borxhi-between-js', function(){
    var personi1_id = $(this).closest('tr').data('personi');
    var personi2_id = $(this).closest('tr').data('bleresi');

    var modal = $('#personaleModal');
    modal.find('.modal-body').html('');
    modal.modal('show');

    $.ajax({
        type: 'post',
        url: path + 'borxhi/between_borxhet',
        data: {personi1_id: personi1_id, personi2_id: personi2_id},
        cache: false,
        success: function(html){
            modal.find('.modal-body').html(html);
        }
    })
})

.on('click', '.personi-status-js', function(){
    var self = $(this);
    var tr = self.closest('tr');
    var personi_id = tr.data('id');
    var status = (parseInt(tr.data('status')) + 1) % 2;

    $.ajax({
        type: 'post',
        url: path + 'personat/toggle_status',
        data: {personi_id: personi_id, status: status},
        dataType: 'json',
        success: function(res){
            if (res.result === 'ok')
            {
                tr.data('status', res.status);
                var remove = (res.status == 1 ? {add: 'alert-success', remove: 'alert-danger'} : {add: 'alert-danger', remove: 'alert-success'});
                self.removeClass(remove.remove);
                self.addClass(remove.add);

                self.text(res.status == 1 ? 'Active' : 'Disabled');
            }
        }
    });
})