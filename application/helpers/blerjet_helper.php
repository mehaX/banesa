<?php
/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 30-Jan-17
 * Time: 10:13 PM
 */

function ka_borxh($produkti_id, $personi_id, $borxhet)
{
    foreach ($borxhet as $borxhi)
        if ($borxhi['produkti_id'] == $produkti_id && $borxhi['personi_id'] == $personi_id)
            return true;
    return false;
}

function getMonth($time, $timestamp = false)
{
    $month = !$timestamp ? intVal(date('n', $time)) : $time;
    $months = array(
        '',
        'Janar',
        'Shkurt',
        'Mars',
        'Prill',
        'Maj',
        'Qershor',
        'Korrik',
        'Gusht',
        'Shtator',
        'Tetor',
        'Nëntor',
        'Dhjetor'
    );

    return $months[$month];
}