<?php
/**
 * Created by PhpStorm.
 * User: mehax
 * Date: 9/8/2017
 * Time: 5:14 PM
 */

function is_guest()
{
    $CI = &get_instance();

    return empty($CI->session->userdata('user_id'));
}

function is_admin()
{
    $CI = &get_instance();

    return $CI->session->userdata('level') == 1;
}

function is_user()
{
    $CI = &get_instance();

    return $CI->session->userdata('level') == 0;
}