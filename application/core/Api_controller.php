<?php
/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 8/19/2017
 * Time: 5:20 PM
 */

class Api_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->is_json_request())
        {
            $res = json_decode(file_get_contents('php://input'), true);
            if ($res)
                $_POST = $res;
        }

        $this->load->database();
    }

    private function display($response)
    {
        $this->output
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, true))
            ->_display();
        exit;
    }

    public function display_success($message)
    {
        $response = ['status' => 'ok', 'message' => $message];
        $this->display($response);
    }
    public function display_error($message)
    {
        $response = ['status' => 'error', 'message' => $message];
        $this->display($response);
    }

    public function display_data($data)
    {
        $response = array_merge(['status' => 'ok', 'result' => $data]);
        $this->display($response);
    }

    public function is_json_request()
    {
        return $this->input->method() == 'post' && $this->input->get_request_header('Content-Type', true) == 'application/json';
    }

    public function get_group_id()
    {
        return $this->input->get('group_id');
    }
}