<?php

/**
 * Created by PhpStorm.
 * User: mehax
 * Date: 9/21/2017
 * Time: 11:13 PM
 */
class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('template');
        $this->load->library('session');

        $this->load->helper('url');
        $this->load->helper('auth');
        $this->load->helper('blerjet');

        $this->load->model('user_model');
    }
}