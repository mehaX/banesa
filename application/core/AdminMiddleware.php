<?php

/**
 * Created by PhpStorm.
 * User: mehax
 * Date: 9/8/2017
 * Time: 5:11 PM
 */
class AdminMiddleware extends UserTemplate
{
    public function __construct($nav = 0)
    {
        parent::__construct($nav);

        if ($this->session->userdata('level') != 1)
            show_404();
    }
}