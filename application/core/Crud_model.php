<?php

/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 19-Jul-17
 * Time: 11:03 AM
 */
class Crud_model extends CI_Model
{
    private $_db;
    protected $table = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

        $this->_db = $this->db;
    }

    public function select($select)
    {
        $this->db->select($select);
        return $this;
    }

    public function group_by($what, $escape = null)
    {
        $this->db->group_by($what, $escape);
        return $this;
    }

    public function where($where, $where2 = null)
    {
        $this->db->where($where, $where2);
        return $this->db;
    }

    public function get($where = [], $all = true)
    {
        $this->_db = $this->db->get_where($this->table, is_array($where) ? $where : ['id' => $where]);
        return $this;
    }

    public function create($row)
    {
        $this->db->insert($this->table, $row);
        return $this->db->insert_id();
    }

    public function update($where, $row)
    {
        $this->db->update($this->table, $row, is_array($where) ? $where : ['id' => $where]);
    }

    public function delete($where)
    {
        $this->db->delete($this->table, is_array($where) ? $where : ['id' => $where]);
    }

    public function all()
    {
        return $this->_db ? $this->_db->result_array() : null;
    }

    public function first()
    {
        return $this->_db ? $this->_db->row_array() : null;
    }
}