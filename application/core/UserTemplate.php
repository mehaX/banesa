<?php

/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 13-Dec-16
 * Time: 11:24 PM
 */
class UserTemplate extends UserMiddleware
{
    public function __construct($nav_tab = 0)
    {
        parent::__construct();

        $this->load->model('chat_model');

        $this->load->helper('blerjet');

        $this->template->set_template('default');
        if ($this->input->is_ajax_request() == false)
        {
            //$msg['messages'] = $this->chat_model->get_chat();

            $this->template->write_view('footer', 'footer');
            $this->template->write_view('header', 'header');
            if ($this->session->userdata('user_id'))
            {
                $this->template->write_view('navbar', 'nav', array('nav' => $nav_tab));
                //$this->template->write_view('msg_modal', 'msg_modal', $msg);
            }
        }
    }
}
