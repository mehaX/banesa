<?php
/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 8/19/2017
 * Time: 9:21 PM
 */

class Api_authmiddleware extends Api_controller
{
    protected $user;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('user_model');

        if (!($token = $this->input->get_post('token')) || !($user = $this->user_model->get(['token' => $token])->first()))
        {
            $this->display_error('Unauthorized');
            return;
        }

        $this->user = $user;
    }
}