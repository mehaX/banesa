<?php

/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 13-Dec-16
 * Time: 11:18 PM
 */
class UserMiddleware extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->user_model->is_user_loggedin() == false)
            redirect('login');
    }
}