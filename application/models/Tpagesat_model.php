<?php

/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 31-Jan-17
 * Time: 10:28 PM
 */
class Tpagesat_model extends CI_Model
{
    public function get_all()
    {
        return $this->db
            ->select('pt.*, p.personi as personi, b.personi as bleresi')
            ->join('personat p', 'p.id=pt.personi_id')
            ->join('personat b', 'b.id=pt.bleresi_id', 'left')
            ->get('pagesat_temp as pt')->result_array();
    }

    public function add($pagesa)
    {
        $this->db->delete('pagesat_temp', array(
            'personi_id' => $pagesa['personi_id'],
            'bleresi_id' => $pagesa['bleresi_id'],
            'lista_id'   => $pagesa['lista_id']
        ));
        $this->db->insert('pagesat_temp', $pagesa);
        return $this->db->insert_id();
    }

    public function delete($personi_id, $bleresi_id = null, $lista_id = null)
    {
        $this->db->delete('pagesat_temp', array('personi_id' => $personi_id, 'bleresi_id' => $bleresi_id, 'lista_id' => $lista_id));
        $this->db->delete('pagesat_temp', array('personi_id' => $bleresi_id, 'bleresi_id' => $personi_id, 'lista_id' => $lista_id));
    }
}