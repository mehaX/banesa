<?php

/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 08-Oct-16
 * Time: 4:27 PM
 */
class User_model extends Crud_model
{
    protected $table = 'personat';

    public function __construct()
    {
        $this->load->database();
        $this->load->library('session');
    }

    public function is_user_loggedin()
    {
        return !empty($this->session->userdata('user_id'));
    }

    public function check_login($user, $pass)
    {
        $this->db->select("id as 'user_id', username, hash, profile, personi, level, status");
        return $this->db->get_where('personat', array('username' => $user, 'password' => $pass, 'status' => 1))->row_array();
    }

    public function get_user($user_id)
    {
        return $this->db
            ->select("id, username, personi, hash")
            ->get_where('personat', array('id' => $user_id))->row_array();
    }
    public function get_user_by_hash($hash)
    {
        return $this->db->select("id as 'user_id', username, hash")->get_where('personat', array('hash' => $hash))->row_array();
    }

    public function update_user($user, $user_id)
    {
        $this->db->update('personat', $user, array('id' => $user_id));
    }

    public function user_unique($username, $user_id)
    {
        return empty($this->db->get_where('personat', array('username' => $username, 'id!=' => $user_id))->num_rows());
    }
}