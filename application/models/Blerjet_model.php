<?php

/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 6/24/2016
 * Time: 7:17 PM
 */
class Blerjet_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function get_personat($all = true)
    {
        if (!$all)
            $this->db->where('status', 1);

        $this->db->order_by('personi', 'asc');
        return $this->db->get('personat')->result_array();
    }

    public function get_blerjet($paguar = 0)
    {
        $this->db
            ->select('b.*, k.personi as krijuar')
            ->from('blerjet b')
            ->join('personat k', 'k.id=b.krijuar_id', 'left');
        if ($paguar !== null)
        {
            $this->db->select("p.personi, p.profile, l.emri as lista");
            $this->db->join('listat l', 'l.id=b.lista_id');
            $this->db->join('personat p', 'p.id=b.personi_id');
            $this->db->where('b.paguar', $paguar);
        }
        else
            $this->db->where('personi_id', null);
        $this->db->order_by('koha', 'desc');

        return $this->db->get()->result_array();
    }

    public function get_blerjet_mujore($month, $year)
    {
        $produktet = $this->db
            ->select('b.koha, p.id as produkti_id, p.emri, p.blerja_id, p.qmimi, b.krijuar_id, bo.total')
            ->from('blerjet b')
            ->join('produktet p', 'p.blerja_id=b.id')
            ->join('(select produkti_id, count(*) as total from borxhet group by produkti_id) bo', 'bo.produkti_id=p.id')
            ->where('b.personi_id', null)
            ->where('month(from_unixtime(b.koha))', $month, false)
            ->where('year(from_unixtime(b.koha))', $year, false)
            ->order_by('p.emri', 'asc')
            ->get()->result_array();

        $produktet_ids = array_unique(array_column($produktet, 'produkti_id'));

        $borxhet = ($produktet_ids ? $this->db
            ->select('b.produkti_id, p.id as personi_id, p.personi, pa.id as paguar')
            ->from('borxhet b')
            ->join('produktet pr', 'pr.id=b.produkti_id')
            ->join('personat p', 'p.id=b.personi_id')
            ->join('pagesat pa', 'pa.blerja_id=pr.blerja_id and pa.personi_id=b.personi_id', 'left')
            ->where_in('produkti_id', $produktet_ids)
            ->get()->result_array() : []);

        return array(
            'produktet' => $produktet,
            'borxhet'   => $borxhet
        );
    }

    public function remove($blerja_id, $remove_blerja = true)
    {
        $this->db->select('id');
        $produktet = $this->db->get_where('produktet', array('blerja_id' => $blerja_id))->result_array();
        $produktet = ($produktet != null ? array_column($produktet, 'id') : array());

        if (!empty($produktet))
        {
            $this->db->where_in('produkti_id', $produktet);
            $this->db->delete('borxhet');
        }

        $this->db->delete('produktet', array('blerja_id' => $blerja_id));

        if ($remove_blerja)
            $this->db->delete('blerjet', array('id' => $blerja_id));
    }

    public function toggle($blerja_id, $paguar)
    {
        $this->db->update('blerjet', array('paguar' => $paguar), array('id' => $blerja_id));
    }

    public function insert($blerja, $produktet)
    {
        $this->db->insert('blerjet', $blerja);
        $blerja_id = $this->db->insert_id();

        if (!empty($produktet)) foreach ($produktet as $produkti)
        {
            $prod = $produkti;
            $prod['blerja_id'] = $blerja_id;
            unset($prod['borxhet']);

            $this->db->insert('produktet', $prod);
            $produkti_id = $this->db->insert_id();

            if ($produkti['borxhet']) foreach ($produkti['borxhet'] as $borxhi)
            {
                $borxh = array(
                    'personi_id'    => $borxhi,
                    'produkti_id'   => $produkti_id
                );
                $this->db->insert('borxhet', $borxh);
            }
        }

        return $blerja_id;
    }

    public function update($blerja, $produktet, $blerja_id)
    {
        $this->db->update('blerjet', $blerja, array('id' => $blerja_id));
        $this->remove($blerja_id, false);

        if (!empty($produktet)) foreach ($produktet as $produkti)
        {
            $prod = $produkti;
            $prod['blerja_id'] = $blerja_id;
            unset($prod['borxhet']);

            $this->db->insert('produktet', $prod);
            $produkti_id = $this->db->insert_id();

            foreach ($produkti['borxhet'] as $borxhi)
            {
                $borxh = array(
                    'personi_id'    => $borxhi,
                    'produkti_id'   => $produkti_id
                );
                $this->db->insert('borxhet', $borxh);
            }
        }
    }

    public function get_blerja($blerja_id, $all = true)
    {
        $blerja = $this->db
            ->select('b.*, p.personi, p.profile, k.personi as krijuar, l.emri as lista')
            ->join('personat p', 'p.id=b.personi_id', 'left')
            ->join('listat l', 'l.id=b.lista_id', 'left')
            ->join('personat k', 'k.id=b.krijuar_id', 'left')
            ->get_where('blerjet b', array('b.id' => $blerja_id))->row_array();
        if (!$all || $blerja == null)
            return $blerja;

        $blerja['produktet'] = $this->db->get_where('produktet', array('blerja_id' => $blerja_id))->result_array();
        if (!empty($blerja['produktet']))
        {
            $ids = array_column($blerja['produktet'], 'id');

            $this->db->select("b.*, e.personi");
            $this->db->join('personat e', 'e.id=b.personi_id', 'left');
            $this->db->where_in('b.produkti_id', $ids);
            $this->db->order_by('e.personi', 'asc');
            $blerja['borxhet'] = $this->db->get('borxhet b')->result_array();
        }

        return $blerja;
    }

    public function get_listat()
    {
        return $this->db->get('listat')->result_array();
    }

    public function llogarit_blerjen($blerja_id)
    {
        $sql = "
            select p.id, p.qmimi*p.sasia as 'totali', count(p.id) as 'count'
            from produktet p
            join borxhet b
            on p.id=b.produkti_id
            where p.blerja_id=$blerja_id
            group by p.id
        ";

        $produktet = $this->db->get_where('produktet', array('blerja_id' => $blerja_id))->result_array();
        if ($produktet == null)
        {
            $produktet = array();
            $borxhet = array();
            goto finish;
        }

        $this->db->select("e.id as 'personi_id', e.personi, sum(p.totali/p.count) as 'borxh', a.id as 'paguar'");
        $this->db->from('personat e');
        $this->db->join('borxhet b', 'e.id=b.personi_id');
        $this->db->join("($sql) p", 'p.id=b.produkti_id');
        $this->db->join('pagesat a', "a.personi_id=e.id and a.blerja_id=$blerja_id", 'left');
        $this->db->group_by('e.id');
        $borxhet = $this->db->get()->result_array();
        if ($borxhet == null)
            $borxhet = array();

        finish:
        return array(
            'produktet' => $produktet,
            'borxhet'   => $borxhet
        );
    }

    public function llogarit_borxhet()
    {
        $sql = "
            select P.id, P.personi, p.bleresi_id, p.bleresi, sum(p.totali) as 'totali', p.lista_id, l.emri as 'lista'
            from personat P
            join (
                select e.id as 'bleresi_id', b.id as 'blerja_id', e.personi as 'bleresi', p.id as 'produkti_id', p.qmimi*p.sasia/p.count as 'totali', p.emri, b.lista_id
                from personat e

                join blerjet b
                on b.personi_id=e.id
                and b.paguar=0

                join (
                    select p.*, count(p.id) as 'count'
                    from produktet p
                    join borxhet b
                    on p.id=b.produkti_id
                    group by p.id
                ) p
                on p.blerja_id=b.id
            ) p
            on P.id != p.bleresi_id
            and (
                select count(*)
                from borxhet B
                where B.personi_id=P.id
                and B.produkti_id=p.produkti_id
            )>0
            and (
                select count(*)
                from pagesat a
                where a.personi_id=P.id
                and a.blerja_id=p.blerja_id
            )=0

            join listat l
            on l.id=p.lista_id
            group by P.id, p.bleresi_id, p.lista_id
        ";

        $borxhet = $this->db->query($sql)->result_array();

        $L = count($borxhet);

        for ($i = 0; $i < $L; $i++) if (isset($borxhet[$i]))
        {
            $b1 = &$borxhet[$i];

            for ($j = $i + 1; $j < $L; $j++) if (isset($borxhet[$j]))
            {
                $b2 = &$borxhet[$j];

                if ($b1['id'] == $b2['bleresi_id'] && $b2['id'] == $b1['bleresi_id'] && $b1['lista_id'] == $b2['lista_id'])
                {
                    $totali = $b1['totali'] - $b2['totali'];
                    $m_totali = abs($totali);

                    /*if ($m_totali < 0.01)
                    {
                        unset($borxhet[$i]);
                        unset($borxhet[$j]);
                        continue;
                    }*/

                    $start = $i;
                    $end = $j;

                    if ($totali < 0)
                    {
                        $end = $i;
                        $start = $j;
                    }

                    $borxhet[$start]['totali'] = $m_totali;
                    unset($borxhet[$end]);

                    if ($totali < 0)
                        break;
                }
            }
        }

        return $borxhet;
    }

    public function llogarit_borxhet_mujore()
    {
        $sql = "
            select PP.id, PP.personi, sum(p.qmimi*p.sasia/p.count) as 'totali'
            from personat PP

            join blerjet b

            join (
                select p.*, count(p.id) as 'count'

                from produktet p
                join borxhet b
                on p.id=b.produkti_id
                group by p.id
            ) p
            on p.blerja_id=b.id

            where b.personi_id is null
            and b.paguar=0
            and (
                select count(*)
                from borxhet B
                where B.personi_id=PP.id
                and B.produkti_id=p.id
            )>0
            and (
                select count(*)
                from pagesat a
                where a.personi_id=PP.id
                and a.blerja_id=p.blerja_id
            )=0

            group by PP.id
        ";

        return $this->db->query($sql)->result_array();
    }

    public function llogarit_borxhet_personale($user_id)
    {
        $sql = "
            select P.id, P.personi, p.bleresi_id, p.bleresi, sum(p.totali) as 'totali'
            from personat P
            join (
                select e.id as 'bleresi_id', b.id as 'blerja_id', e.personi as 'bleresi', p.id as 'produkti_id', p.qmimi*p.sasia/p.count as 'totali', p.emri
                from personat e

                join blerjet b
                on b.personi_id=e.id
                and b.paguar=0

                join (
                    select p.*, count(p.id) as 'count'
                    from produktet p
                    join borxhet b
                    on p.id=b.produkti_id
                    group by p.id
                ) p
                on p.blerja_id=b.id
            ) p
            on P.id != p.bleresi_id
            and (
                select count(*)
                from borxhet B
                where B.personi_id=P.id
                and B.produkti_id=p.produkti_id
            )>0
            and (
                select count(*)
                from pagesat a
                where a.personi_id=P.id
                and a.blerja_id=p.blerja_id
            )=0

            where P.id=$user_id or p.bleresi_id=$user_id
            group by P.id, p.bleresi_id
        ";

        $borxhet = $this->db->query($sql)->result_array();

        $L = count($borxhet);

        for ($i = 0; $i < $L; $i++) if (isset($borxhet[$i]))
        {
            $b1 = &$borxhet[$i];

            for ($j = $i + 1; $j < $L; $j++) if (isset($borxhet[$j]))
            {
                $b2 = &$borxhet[$j];

                if ($b1['id'] == $b2['bleresi_id'] && $b2['id'] == $b1['bleresi_id'])
                {
                    $totali = $b1['totali'] - $b2['totali'];
                    $m_totali = abs($totali);

                    /*if ($m_totali < 0.01)
                    {
                        unset($borxhet[$i]);
                        unset($borxhet[$j]);
                        continue;
                    }*/

                    $start = $i;
                    $end = $j;

                    if ($totali < 0)
                    {
                        $end = $i;
                        $start = $j;
                    }

                    $borxhet[$start]['totali'] = $m_totali;
                    unset($borxhet[$end]);

                    if ($totali < 0)
                        break;
                }
            }
        }

        return $borxhet;
    }

    public function between_borxhet($personi1_id, $personi2_id)
    {
        return $this->db->query("
            select p.*, b.koha, PP.personi as personi, PP.id as personi_id, BB.personi as bleresi, BB.id as bleresi_id, (p.qmimi*p.sasia)/p.total_count as totali
            from (
                select p.*, count(p.id) as total_count
                from produktet p
                join borxhet b on b.produkti_id=p.id
                group by p.id
            ) p
            join borxhet o on o.produkti_id=p.id
            join blerjet b on b.id=p.blerja_id
            join personat BB on BB.id=b.personi_id
            join personat PP on PP.id=o.personi_id


            where b.paguar=0
            and ((b.personi_id=$personi1_id and o.personi_id=$personi2_id) or (b.personi_id=$personi2_id and o.personi_id=$personi1_id))
            and (b.id, o.personi_id) not in (select blerja_id, personi_id from pagesat)
            order by b.koha desc
        ")->result_array();
    }

    public function between_borxhet_mujore($personi_id)
    {
        return $this->db->query("
            select p.*, b.koha, PP.personi as personi, PP.id as personi_id, (p.qmimi*p.sasia)/p.total_count as totali
            from (
                select p.*, count(p.id) as total_count
                from produktet p
                join borxhet b on b.produkti_id=p.id
                group by p.id
            ) p
            join borxhet o on o.produkti_id=p.id
            join blerjet b on b.id=p.blerja_id
            join personat PP on PP.id=o.personi_id


            where b.paguar=0
            and b.personi_id is null
            and o.personi_id=$personi_id
            and (b.id, o.personi_id) not in (select blerja_id, personi_id from pagesat)
            order by b.koha desc
        ")->result_array();
    }

    public function paguaj_toggle($personi_id, $blerja_id, $paguaj = true)
    {
        $arr = array(
            'personi_id' => $personi_id,
            'blerja_id'  => $blerja_id
        );

        $this->db->delete('pagesat', $arr);

        if ($paguaj)
            $this->db->insert('pagesat', $arr);
    }

    public function paguaj_borxhin_total($personi_id, $bleresi_id, $lista_id)
    {
        $this->db->select("id");
        $this->db->from('blerjet');
        if ($lista_id != 0)
        {
            $this->db->where('personi_id', $bleresi_id);
            $this->db->where('lista_id', $lista_id);
        }
        else
            $this->db->where('personi_id', null);
        $this->db->where('paguar', 0);
        $this->db->where_not_in('id', "
            select blerja_id
            from pagesat
            where personi_id=$personi_id
        ", false);
        $blerjet = $this->db->get()->result_array();

        if (!empty($blerjet)) foreach ($blerjet as $blerja)
            $this->db->insert('pagesat', array('personi_id' => $personi_id, 'blerja_id' => $blerja['id']));

        if ($lista_id == 0)
            return;

        $this->db->select("id");
        $this->db->from('blerjet');
        $this->db->where('personi_id', $personi_id);
        $this->db->where('lista_id', $lista_id);
        $this->db->where('paguar', 0);
        $this->db->where_not_in('id', "
            select blerja_id
            from pagesat
            where personi_id=$bleresi_id
        ", false);
        $blerjet = $this->db->get()->result_array();

        if (!empty($blerjet)) foreach ($blerjet as $blerja)
            $this->db->insert('pagesat', array('personi_id' => $bleresi_id, 'blerja_id' => $blerja['id']));
    }
}