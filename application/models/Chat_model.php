<?php

/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 14-Dec-16
 * Time: 1:39 AM
 */
class Chat_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_chat()
    {
        $res = $this->db
            ->select('c.*, p.personi, p.username, p.profile')
            ->from('chat c')
            ->join('personat p', 'p.id=c.from_id')
            ->order_by('time', 'asc')
            ->get()->result_array();
        $msg = array();
        while (!empty($res))
        {
            $row = array_pop($res);
            if (empty($msg) || $msg[0]['from_id'] != $row['from_id'])
                array_unshift($msg, array(
                    'from_id'   => $row['from_id'],
                    'username'  => $row['username'],
                    'personi'   => $row['personi'],
                    'profile'   => $row['profile'],
                    'messages'  => array()
                ));
            array_unshift($msg[0]['messages'], array(
                'msg'   => $row['msg'],
                'time'  => $row['time']
            ));
        }
        return $msg;
    }

    public function post_message($msg)
    {
        $this->db->insert('chat', $msg);
        return $this->db->insert_id();
    }
}