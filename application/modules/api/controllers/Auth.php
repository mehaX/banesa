<?php
/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 8/19/2017
 * Time: 9:34 PM
 */

class Auth extends Api_controller
{
    private $app_hash = '}|D9$4cX;wd15;H';

    public function __construct()
    {
        parent::__construct();

        $this->load->model('user_model');
    }

    public function login()
    {
        if ($this->input->method() == 'post')
        {
            $data['username'] = $this->input->post('username');
            $data['password'] = sha1($this->input->post('password') . $this->app_hash);

            if ($user = $this->user_model->get($data)->first())
            {
                $new_token = sha1(date('Y-m-d H:i:s') . microtime() . $this->app_hash);
                $this->user_model->update($user['id'], array('token' => $new_token));
                $this->display_data(['token' => $new_token]);
            }
        }

        $this->display_error('Unauthorized');
    }
}