<?php
/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 8/19/2017
 * Time: 9:11 PM
 */

class Home extends Api_controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->display_success('alive');
    }

    public function is404()
    {
        if ($this->uri->segment(1) == 'api')
            $this->display_error("Error 404");

        show_404();
    }
}