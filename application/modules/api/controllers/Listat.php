<?php
/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 8/19/2017
 * Time: 10:00 PM
 */

class Listat extends Api_authmiddleware
{
    public function index()
    {
        $this->load->model('lista_model');

        $data = $this->lista_model->get()->all();

        $this->display_data($data);
    }
}