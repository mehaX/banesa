<?php
/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 8/19/2017
 * Time: 9:24 PM
 */

class Blerjet extends Api_authmiddleware
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('blerjet_model');
    }

    public function index($paguar = 0)
    {
        $user_id = $this->user['id'];

        $data = $this->blerjet_model->get_blerjet($paguar);

        $this->display_data($data);
    }

    public function blerja($blerja_id = null)
    {
        if ($blerja_id != null && ($data = $this->blerjet_model->get_blerja($blerja_id)) == null)
            $this->display_error('404 Not found');

        $this->display_data($data);
    }
}