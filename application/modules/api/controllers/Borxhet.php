<?php
/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 8/19/2017
 * Time: 10:07 PM
 */

class Borxhet extends Api_authmiddleware
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('blerjet_model');
    }

    public function index()
    {
        $data = $this->blerjet_model->llogarit_borxhet();

        $this->display_data($data);
    }

    public function mujore()
    {
        $data = $this->blerjet_model->llogarit_borxhet_mujore();

        $this->display_data($data);
    }

    public function personale()
    {
        $data = $this->blerjet_model->llogarit_borxhet_personale($this->user['id']);

        $this->display_data($data);
    }
}