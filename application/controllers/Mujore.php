<?php

/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 09-Oct-16
 * Time: 6:09 PM
 */
class Mujore extends UserTemplate
{
    public function __construct()
    {
        parent::__construct(2);

        $this->load->model('blerjet_model');
    }

    public function landing()
    {
        $year = $this->input->get('year');
        if (!$year)
            $year = date('Y');

        $data['c_year'] = $year;

        $this->template->write_view('content', 'mujoret-calendar', $data);
        $this->template->render();
    }

    public function llogarit()
    {
        $year = $this->input->post('year');
        $month = $this->input->post('month');

        $data = $this->blerjet_model->get_blerjet_mujore($month, $year);
        $data['personi_id'] = $this->session->userdata('user_id');

        $html = $this->load->view('muaji-llogarit', $data, true);
        echo json_encode(['content' => $html]);
    }

    public function addedit($blerja_id = null)
    {
        if ($blerja_id != null && (($data['blerja'] = $this->blerjet_model->get_blerja($blerja_id)) == null || $data['blerja']['personi_id'] != null))
            show_404();

        $year = $this->input->get('year');
        $month = $this->input->get('month');

        $data['year'] = $year ? $year : date('Y');
        $data['month'] = $month ? $month : date('n');

        $data['personat']   = $this->blerjet_model->get_personat(false);
        $data['listat']     = $this->blerjet_model->get_listat();

        $this->template->write_view('content', 'mujore-addedit', $data);
        $this->template->render();
    }

    public function submit($blerja_id = null)
    {
        if ($blerja_id != null && (($bl = $this->blerjet_model->get_blerja($blerja_id, false)) == null || $bl['personi_id'] != null))
            show_404();

        $blerja['koha'] = strtotime($this->input->post('viti').'-'.$this->input->post('muaji').'-01');
        $blerja['krijuar_id'] = $this->session->userdata('user_id');
        $produktet      = $this->input->post('produktet');

        if ($blerja_id == null)
            $this->blerjet_model->insert($blerja, $produktet);
        else
            $this->blerjet_model->update($blerja, $produktet, $blerja_id);

        echo json_encode(array('path' => base_url('mujoret')));
    }
}