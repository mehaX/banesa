<?php

/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 14-Dec-16
 * Time: 2:14 AM
 */
class Chat extends UserMiddleware
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('chat_model');
    }

    public function post_new_message()
    {
        $msg = $this->input->post('msg');
        $msg = str_replace("&", "&amp", $msg);
        $msg = str_replace("<", "&lt;", $msg);
        $msg = str_replace(">", "&gt;", $msg);
        $msg = str_replace("\n", "<br>", $msg);

        $message = array(
            'from_id'   => $this->session->userdata('user_id'),
            'msg'       => $msg,
            'time'      => time()
        );

        $msg_id = $this->chat_model->post_message($message);
        echo json_encode(array(
            'id'    => $msg_id,
            'msg'   => $msg
        ));
    }
}