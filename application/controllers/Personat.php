<?php

/**
 * Created by PhpStorm.
 * User: mehax
 * Date: 9/8/2017
 * Time: 5:12 PM
 */
class Personat extends AdminMiddleware
{
    public function __construct()
    {
        parent::__construct(5);
    }

    public function index()
    {
        $data['users'] = $this->user_model->get(['level' => 0])->all();

        $this->template->write_view('content', 'personat-landing', $data);
        $this->template->render();
    }

    public function toggle_status()
    {
        $personi_id = $this->input->post('personi_id');
        $status = $this->input->post('status');

        $this->user_model->update($personi_id, ['status' => $status]);

        echo json_encode(['result' => 'ok', 'status' => $status]);
    }
}