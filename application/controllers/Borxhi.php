<?php

/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 6/27/2016
 * Time: 5:08 AM
 */
class Borxhi extends UserTemplate
{
    public function __construct()
    {
        parent::__construct(3);
        $this->load->model('blerjet_model');
        $this->load->model('tpagesat_model');
    }

    public function index()
    {
        $data['my_id']      = $this->session->userdata('user_id');
        $data['user_id']    = $data['my_id'];
        $data['personi']    = $this->session->userdata('personi');

        $data['borxhet']    = $this->blerjet_model->llogarit_borxhet();
        $data['mujore']     = $this->blerjet_model->llogarit_borxhet_mujore();
        $data['personale']  = $this->blerjet_model->llogarit_borxhet_personale($data['user_id']);
        $data['pagesat']    = $this->tpagesat_model->get_all();

        $data['listat']     = $this->blerjet_model->get_listat();

        $this->template->write_view('content', 'borxhet', $data);
        $this->template->render();
    }

    public function paguaj()
    {
        $personi_id = $this->input->post('personi_id');
        $blerja_id  = $this->input->post('blerja_id');
        $paguaj     = $this->input->post('paguaj');

        $blerja     = $this->blerjet_model->get_blerja($blerja_id, false);
        if ($blerja == null)
            show_404();
        $bleresi_id = $blerja['personi_id'];

        if (!empty($personi_id) && !empty($blerja_id))
        {
            /*if ($paguaj == 'true')
                $this->tpagesat_model->delete($personi_id, $bleresi_id, $blerja['lista_id']);*/

            $this->blerjet_model->paguaj_toggle($personi_id, $blerja_id, $paguaj == 'true');
        }

        $blerja = $this->blerjet_model->llogarit_blerjen($blerja_id);
        echo number_format(array_sum(array_map(function($val) use($bleresi_id) {return ($val['personi_id'] != $bleresi_id && $val['paguar'] == null ? $val['borxh'] : 0);}, $blerja['borxhet'])), 2);
    }

    public function paguaj_totalin()
    {
        $this->output->enable_profiler();

        $personi_id = $this->input->post('personi_id');
        $bleresi_id = $this->input->post('bleresi_id');
        $lista_id   = $this->input->post('lista_id');
        $total      = $this->input->post('total');
        $shuma      = (!$total ? $this->input->post('shuma') : null);

        if ($total == "false")
        {
            $shuma = $this->input->post('shuma');
            $tpagesa['personi_id'] = $personi_id;

            if ($lista_id != '0')
            {
                $tpagesa['bleresi_id'] = $bleresi_id;
                $tpagesa['lista_id'] = $lista_id;
            }
            else
            {
                $tpagesa['bleresi_id'] = null;
                $tpagesa['lista_id'] = null;
                $lista_id = null;
            }

            if ($shuma)
            {
                $tpagesa['shuma'] = $shuma;
                $this->tpagesat_model->add($tpagesa);
            }
            else
                $this->tpagesat_model->delete($personi_id, $bleresi_id, $lista_id);
        }
        else
        {
            if ($lista_id != '0')
                $this->tpagesat_model->delete($personi_id, $bleresi_id, $lista_id);
            else
                $this->tpagesat_model->delete($personi_id);

            $this->blerjet_model->paguaj_borxhin_total($personi_id, $bleresi_id, $lista_id);
        }
    }

    public function personale_klienti($klienti_id)
    {
        if ($klienti_id == null || ($personi = $this->user_model->get_user($klienti_id)) == null)
            show_404();

        $data['my_id']      = $this->session->userdata('user_id');
        $data['user_id']    = $klienti_id;
        $data['personi']    = $personi['personi'];

        $data['borxhet']    = $this->blerjet_model->llogarit_borxhet();
        $data['mujore']     = $this->blerjet_model->llogarit_borxhet_mujore();
        $data['personale']  = $this->blerjet_model->llogarit_borxhet_personale($klienti_id);
        $data['pagesat']    = $this->tpagesat_model->get_all();

        $data['listat']     = $this->blerjet_model->get_listat();

        $this->load->view('personale', $data);
    }

    public function between_borxhet()
    {
        $personi1_id = $this->input->post('personi1_id');
        $personi2_id = $this->input->post('personi2_id');

        if ($personi1_id == null)
            show_404();

        $data['user_id'] = $this->session->userdata('user_id');
        $data['betweens'] = $personi2_id ? $this->blerjet_model->between_borxhet($personi1_id, $personi2_id) : $this->blerjet_model->between_borxhet_mujore($personi1_id);

        $this->load->view($personi2_id ? 'between-borxhet' : 'between-borxhet-mujore', $data);
    }
}