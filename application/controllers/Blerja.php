<?php

/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 6/25/2016
 * Time: 1:00 AM
 */
class Blerja extends UserTemplate
{
    public function __construct()
    {
        parent::__construct(1);
        $this->load->model('blerjet_model');
    }

    public function landing($status = 0)
    {
        $data['blerjet']    = $this->blerjet_model->get_blerjet($status);
        $data['pill']       = $status + 1;
        $data['user_id']    = $this->session->userdata('user_id');

        $this->template->write_view('content', "index", $data, array());
        $this->template->render();
    }

    public function addedit($blerja_id = null)
    {
        if ($blerja_id != null && ($data['blerja'] = $this->blerjet_model->get_blerja($blerja_id)) == null)
            show_404();

        $data['personat']   = $this->blerjet_model->get_personat(false);
        $data['listat']     = $this->blerjet_model->get_listat();
        $data['user_id']    = $this->session->userdata('user_id');

        $this->template->write_view('content', 'blerja-addedit', $data);
        $this->template->render();
    }

    public function submit($blerja_id = null)
    {
        $user_id = $this->session->userdata('user_id');
        if ($blerja_id != null && ($blerja = $this->blerjet_model->get_blerja($blerja_id, false)) == null)
            show_404();

        $blerja = array(
            'personi_id' => $this->input->post('personi_id'),
            'lista_id'   => $this->input->post('lista'),
            'koha'       => time(),
            'krijuar_id' => $user_id
        );
        $produktet = $this->input->post('produktet');

        if ($blerja_id == null)
            $this->blerjet_model->insert($blerja, $produktet);
        else
            $this->blerjet_model->update($blerja, $produktet, $blerja_id);

        echo json_encode(array('path' => base_url()));
    }

    public function paguar($blerja_id, $paguar = 1)
    {
        if ($paguar != 1 && $paguar != 0)
            show_404();

        $this->blerjet_model->toggle($blerja_id, $paguar);
    }

    public function llogarit($blerja_id)
    {
        if ($blerja_id == null || ($blerja = $this->blerjet_model->get_blerja($blerja_id)) == null)
        {
            echo "Error 404";
            return;
        }

        $data = $this->blerjet_model->llogarit_blerjen($blerja_id);
        $data['personi_id'] = $blerja['personi_id'];
        $data['blerja']     = $blerja;
        $data['user_id']    = $this->session->userdata('user_id');

        echo json_encode(array('content' => $this->load->view('blerja-body', $data, true)));
    }
}
?>