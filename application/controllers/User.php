<?php

/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 08-Oct-16
 * Time: 4:32 PM
 */
class User extends MY_Controller
{
    private $app_hash = '}|D9$4cX;wd15;H';

    public function __construct()
    {
        parent::__construct();

        $this->load->helper('form');
    }

    public function login()
    {
        if ($this->user_model->is_user_loggedin())
            redirect();

        $this->template->write_view('header', 'header');
        $this->template->write_view('content', 'login');
        $this->template->render();
    }

    public function login_submit()
    {
        if ($this->user_model->is_user_loggedin())
            redirect();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', '', 'required', array('required' => 'Emri i perdoruesit eshte i kerkuar'));
        $this->form_validation->set_rules('password', '', 'required|callback_check_login', array(
            'required' => 'Fjalekalimi eshte i kerkuar',
            'check_login' => 'Kombinimi i gabueshem'
        ));

        if ($this->form_validation->run())
            redirect();

        $this->login();
    }

    public function check_login()
    {
        $username = $this->input->post('username');
        $password = sha1($this->input->post('password') . $this->app_hash);

        $user = $this->user_model->check_login($username, $password);
        if ($user == null)
            return false;

        $this->session->set_userdata($user);
        return true;
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

    public function new_password($hash)
    {
        $person = $this->user_model->get_user_by_hash($hash);
        if ($person == null)
            show_404();

        $this->template->write_view('header', 'header');
        $this->template->write_view('navbar', 'nav', array('nav' => 0));
        $this->template->write_view('content', 'new-password', array('person' => $person));
        $this->template->write_view('footer', 'footer');
        $this->template->render();
    }
    public function new_password_submit($hash)
    {
        $person = $this->user_model->get_user_by_hash($hash);
        $this->user_id = $person['user_id'];
        if ($person == null)
            show_404();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', '', 'required|callback_user_unique[username]', array(
            'required'      => 'Emri i perdoruesit eshte i kerkuar',
            'user_unique'   => 'Ky perdorues ekziston'
        ));
        $this->form_validation->set_rules('password', '', 'required|min_length[6]|max_length[20]', array(
            'required'      => 'Fjalekalimi eshte i kerkuar',
            'min_length'    => 'Fjalekalimi duhet ti kete se paku 6 karaktere',
            'max_length'    => 'Fjalekalimi duhet ti kete me se shumti 20 karaktere'
        ));

        if ($this->form_validation->run())
        {
            $user = array(
                'username' => $this->input->post('username'),
                'password' => sha1($this->input->post('password') . $this->app_hash)
            );
            $this->user_model->update_user($user, $person['user_id']);

            $this->session->set_userdata(array(
                'user_id' => $person['user_id'],
                'username' => $user['username'],
                'hash' => $hash
            ));
            redirect();
        }

        $this->new_password($hash);
    }

    public function user_unique($username)
    {
        return $this->user_model->user_unique($username, $this->user_id);
    }
}