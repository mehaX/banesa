<?php
/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 6/25/2016
 * Time: 1:14 AM
 */
?>

<?php
if (!isset($produkti))
    $produkti = array(
        'emri' => '',
        'qmimi' => '',
        'sasia' => '1'
    );
if (!isset($borxhet))
    $borxhet = array();
$is_mujore = isset($is_mujore);
$user_id = $this->session->userdata('user_id');
$row_id = microtime();
?>

<tr class="produkti-row-js">
    <td>
        <input type="text" class="form-control emri-js" placeholder="Emri" value="<?php echo $produkti['emri']; ?>" />
    </td>

    <td>
        <input type="text" class="form-control qmimi-js" placeholder="Çmimi" value="<?php echo $produkti['qmimi']; ?>" />
    </td>

    <?php if (!$is_mujore): ?>
    <td style="padding:0"><span class="glyphicon glyphicon-remove hidden-xs" style="margin-top: 17px;"></span></td>

    <td>
        <div class="input-group number-spinner">
            <span class="input-group-btn data-dwn">
				<button class="btn btn-default" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></button>
			</span>
            <input type="text" class="form-control text-center sasia-js" value="<?php echo $produkti['sasia']; ?>" min="0" max="40" />
            <span class="input-group-btn data-up">
				<button class="btn btn-default" data-dir="up"><span class="glyphicon glyphicon-plus"></span></button>
			</span>
        </div>
    </td>
    <?php endif; ?>

    <?php foreach ($personat as $personi): ?>
        <td class="borxhet-js hidden-xs text-center">
            <div class="new-checkbox checkbox-js btn-group" data-toggle="buttons">
                <label data-personi="<?php echo $personi['id']; ?>" class="btn btn-sm btn-checkbox btn-<?php echo ($personi['id'] == $user_id ? 'warning' : 'default'); ?> <?php if ((isset($produkti['id']) && ka_borxh($produkti['id'], $personi['id'], $borxhet)) || ($is_mujore && !isset($blerja))) echo 'active'; ?>">
                    <input type="checkbox" autocomplete="off" checked>
                    <span class="glyphicon glyphicon-ok"></span>
                </label>
            </div>
        </td>
    <?php endforeach; ?>
    <td>
        <span class="btn btn-sm glyphicon glyphicon-minus-sign fshij-produkt-js hidden-xs <?php if (!$delete) echo 'hidden'; ?>" id="<?php if (isset($produkti['id'])) echo 'produkti-id'; ?>"></span>
        <div class="dropup dropdown-js">
            <span class="btn btn-sm glyphicon glyphicon-chevron-down dropdown-toggle visible-xs" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false"></span>
            <div data-container="body" class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-<?php if (isset($produkti['id'])) echo 'produkti-id'; ?>">
                <div class="funkyradio">
                    <?php foreach ($personat as $personi): ?>
                    <div class="funkyradio-<?php echo ($personi['id'] == $user_id ? 'warning' : 'info'); ?>">
                        <input type="checkbox" name="personi-<?php echo $personi['id']; ?>" id="personi-<?php echo $row_id; ?>-<?php echo $personi['id']; ?>" data-personi="<?php echo $personi['id']; ?>" <?php if ((isset($produkti['id']) && ka_borxh($produkti['id'], $personi['id'], $borxhet)) || ($is_mujore && !isset($blerja))) echo 'checked'; ?> />
                        <label for="personi-<?php echo $row_id; ?>-<?php echo $personi['id']; ?>"><?php echo $personi['personi']; ?></label>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </td>
</tr>