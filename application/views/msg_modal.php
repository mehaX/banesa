<?php
/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 13-Dec-16
 * Time: 11:26 PM
 */
$user_id = $this->session->userdata('user_id');
?>
<div id="msg_popup" class="panel panel-default msg-collapsed">
    <div class="panel-heading">
        <h3 class="panel-title">Chat</h3>
    </div>

    <div class="panel-body">
        <?php foreach ($messages as $message) if ($message['from_id'] == $user_id): ?>
        <div class="row msg msg-right" data-user_id="<?php echo $message['from_id']; ?>">
            <div class="col-xs-10 messages">
                <ul>
                    <?php foreach ($message['messages'] as $msg): ?>
                        <li>
                            <p><?php echo $msg['msg']; ?></p>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-xs-2 profile">
                <img src="<?php echo base_url(array('uploads', 'users', 'thumbnails', $message['profile'])); ?>">
            </div>
        </div>
        <?php else: ?>
        <div class="row msg msg-left" data-user_id="<?php echo $message['from_id']; ?>">
            <div class="col-xs-2 profile">
                <img src="<?php echo base_url(array('uploads', 'users', 'thumbnails', $message['profile'])); ?>">
            </div>
            <div class="col-xs-10 messages">
                <ul>
                    <?php foreach ($message['messages'] as $msg): ?>
                        <li>
                            <p><?php echo $msg['msg']; ?></p>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <?php endif; ?>
    </div>

    <div class="panel-footer">
        <div class="input-group post-message-js">
            <input type="text" class="form-control" placeholder="Type message" />
            <span class="input-group-btn">
                <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-send"></span></button>
            </span>
        </div>
    </div>
</div>

<script>
    var panel_body = $('#msg_popup .panel-body');
    panel_body.scrollTop(panel_body.prop("scrollHeight"));
</script>
