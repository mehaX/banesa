<div class="container">
    <table class="table table-responsive table-striped table-hover">
        <thead><tr>
            <th>Status</th>
            <th>Emri</th>
            <th>Username</th>
            <th></th>
        </tr></thead>

        <tbody>
        <?php foreach($users as $user): ?>
            <tr data-id="<?php echo $user['id'] ?>" data-status="<?php echo $user['status'] ?>">
                <td><span class="label label-<?php echo $user['status'] == 1 ? 'success' : 'danger' ?> personi-status-js" style="cursor: pointer"><?php echo $user['status'] == 1 ? 'Active' : 'Disabled' ?></span></td>
                <td><?php echo $user['personi'] ?></td>
                <td><?php echo $user['username'] ?></td>
                <td>
                </td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
</div>