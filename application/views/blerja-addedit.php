<?php
/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 6/25/2016
 * Time: 12:49 AM
 */
?>

<div class="personat-template-js hidden">
    <ul class="personi-template-js list-group">
        <?php foreach ($personat as $personi): ?>
            <li class="list-group-item">
                <div class="checkbox">
                    <label>
                        <input data-personi="<?php echo $personi['id']; ?>" type="checkbox" <?php if (isset($produkti['id']) && ka_borxh($produkti['id'], $personi['id'], $borxhet)) echo 'checked'; ?> />
                        <?php echo $personi['personi']; ?>
                    </label>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>

<div class="container" data-mujore="false">
    <a href="<?php echo base_url(); ?>" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span> Pas</a>

    <hr>

    <div class="row">
        <div class="col-lg-10">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Blerja</h3>
                </div>

                <div class="panel-body">
                    <table class="table produktet-js">
                        <thead><tr>
                            <th><span class="pull-right">Blerësi:</span></th>
                            <th colspan="2">
                                <select class="form-control blerja-personi-js">
                                    <?php foreach ($personat as $personi): ?>
                                        <option value="<?php echo $personi['id']; ?>" <?php if ((isset($blerja) && $blerja['personi_id'] == $personi['id']) || (!isset($blerja) && $personi['id'] == $this->session->userdata('user_id'))) echo 'selected'; ?>><?php echo $personi['personi']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </th>
                            <th>
                                <select class="form-control lista-js">
                                    <?php foreach ($listat as $lista): ?>
                                        <option value="<?php echo $lista['id']; ?>" <?php if (isset($blerja) && $blerja['lista_id'] == $lista['id']) echo 'selected'; ?>><?php echo $lista['emri']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </th>
                            <?php foreach ($personat as $personi): ?>
                            <th class="hidden-xs"><?php echo $personi['personi']; ?></th>
                            <?php endforeach; ?>
                            <th></th>
                        </tr></thead>

                        <?php
                        if (isset($blerja['produktet']) && !empty($blerja['produktet'])) foreach ($blerja['produktet'] as $key => $produkti)
                            $this->load->view('blerja-produkti-row', array('personat' => $personat, 'produkti' => $produkti, 'borxhet' => $blerja['borxhet'], 'delete' => ($key > 0)));
                        else
                            $this->load->view('blerja-produkti-row', array('personat' => $personat, 'delete' => false));
                        ?>
                    </table>

                    <button class="btn btn-sm btn-info shto-produkt-js"><span class="glyphicon glyphicon-plus"></span> Shto rresht</button>
                    <button class="btn btn-sm btn-success pull-right blerja-submit-js"><span class="glyphicon glyphicon-save"></span> Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
