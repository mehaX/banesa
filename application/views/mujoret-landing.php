<?php
/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 09-Oct-16
 * Time: 6:12 PM
 */
?>
<div class="container">
    <div class="row">
        <div class="col-xs-8">
            <ul class="nav nav-pills">
                <li role="presentation" class="active"><a href="<?php echo base_url('mujoret?viti=2016'); ?>">2016</a></li>
            </ul>
        </div>

        <div class="col-xs-4">
            <div class="pull-right">
                <a href="<?php echo base_url('mujore-add'); ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Shto</a>
            </div>
        </div>
    </div>

    <hr />

    <div class="well">
        <?php foreach ($blerjet as $blerja): ?>
            <div class="blerja-row" data-id="<?php echo $blerja['id']; ?>" data-status="<?php echo $blerja['paguar']; ?>">
                <div class="media">
                    <div class="media-body">
                        <h4 class="media-heading"><strong><?php echo getMonth($blerja['koha']).date(' Y', $blerja['koha']); ?></strong></h4>
                        <?php if ($blerja['krijuar']): ?>Nga <strong class="<?php if ($blerja['krijuar_id'] == $user_id) echo 'text-info'; ?>"><?php echo $blerja['krijuar']; ?></strong><?php endif; ?>
                    </div>

                    <div class="media-right dropdown-user">
                        <div class="llogarit-blerjen-js">
                            <span class="glyphicon glyphicon-chevron-down"></span>
                        </div>
                    </div>
                </div>

                <div class=" user-infos" style="display:none">
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <a class="btn btn-sm btn-warning" href="<?php echo base_url(array('mujore-edit', $blerja['id'])); ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                        </div>
                    </div>

                    <br>

                    <div class="blerja-body-js" style="display:none">

                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>