<?php
/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 6/27/2016
 * Time: 5:09 AM
 */
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6">
            <?php $this->load->view('personale'); ?>
        </div>
    </div>

    <hr>

    <div class="col-md-6">
    <?php foreach ($listat as $lista): ?>
        <div class="panel panel-default" data-lista="<?php echo $lista['id']; ?>">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $lista['emri']; ?></h3>
            </div>

            <table class="table table-striped">
                <thead><tr>
                    <th>Personi</th>
                    <th>Blerësi</th>
                    <th>Paguar</th>
                    <th>Shuma</th>
                    <th></th>
                </tr></thead>

                <?php $b_borxhet = array_filter($borxhet, function($borxhi) use($lista) {return $borxhi['lista_id'] == $lista['id'];}); foreach ($b_borxhet as $borxhi): ?>
                    <?php
                    $pagesa = array_filter($pagesat, function($pagesa) use($borxhi, $lista){
                        return $pagesa['personi_id'] == $borxhi['id'] && $pagesa['bleresi_id'] == $borxhi['bleresi_id'] && $pagesa['lista_id'] == $lista['id'];
                    });
                    if ($pagesa)
                        $pagesa = array_shift($pagesa);
                    ?>
                <tr data-bleresi="<?php echo $borxhi['bleresi_id'] ?>" data-personi="<?php echo $borxhi['id']; ?>">
                    <td class="<?php if ($borxhi['id'] == $user_id): ?>text-info<?php endif; ?> personale-js" data-id="<?php echo $borxhi['id']; ?>"><?php echo $borxhi['personi']; ?></td>
                    <td class="<?php if ($borxhi['bleresi_id'] == $user_id): ?>text-info<?php endif; ?> personale-js" data-id="<?php echo $borxhi['bleresi_id']; ?>"><?php echo $borxhi['bleresi']; ?></td>
                    <td><span class="paguar" <?php if (!$pagesa): ?>style="display: none"<?php endif; ?>><span class="value"><?php if ($pagesa) echo number_format($pagesa['shuma'], 2); ?></span> €</span></td>
                    <td class="<?php if ($borxhi['bleresi_id'] == $user_id) echo 'text-success'; else if ($borxhi['id'] == $user_id) echo 'text-danger'; ?>"><?php echo number_format($borxhi['totali'], 2); ?>€</td>
                    <td class="text-right">
                        <div class="btn-group">
                            <button class="btn btn-xs btn-info borxhi-between-js"><span class="glyphicon glyphicon-list"></span></button>
                            <button class="btn btn-xs btn-success borxhi-paguaj-js"><span class="glyphicon glyphicon-ok"></span> <span class="hidden-xs">Paguaj</span></button>
                        </div>
                    </td>
                </tr>
                <?php endforeach; unset($b_borxhet); ?>
            </table>
        </div>
    <?php endforeach; ?>
    </div>

    <div class="col-md-6">
        <div class="panel panel-danger" data-lista="0">
            <div class="panel-heading">
                <h3 class="panel-title">Borxhet Mujore</h3>
            </div>

            <table class="table table-striped">
                <thead><tr>
                    <th>Personi</th>
                    <th>Paguar</th>
                    <th>Shuma</th>
                    <th></th>
                </tr></thead>

                <?php foreach ($mujore as $borxhi): ?>
                    <?php
                    $pagesa = array_filter($pagesat, function($pagesa) use($borxhi){
                        return $pagesa['personi_id'] == $borxhi['id'] && $pagesa['bleresi_id'] == null && $pagesa['lista_id'] == null;
                    });
                    if ($pagesa)
                        $pagesa = array_shift($pagesa);
                    ?>
                    <tr data-personi="<?php echo $borxhi['id']; ?>">
                        <td class="<?php if ($borxhi['id'] == $user_id): ?>text-info<?php endif; ?> personale-js" data-id="<?php echo $borxhi['id']; ?>"><?php echo $borxhi['personi']; ?></td>
                        <td><span class="paguar" <?php if (!$pagesa): ?>style="display: none"<?php endif; ?>><span class="value"><?php if ($pagesa) echo number_format($pagesa['shuma'], 2); ?></span> €</span></td>
                        <td class="<?php if ($borxhi['id'] == $user_id) echo 'text-danger'; ?>"><?php echo number_format($borxhi['totali'], 2); ?> €</td>
                        <td class="text-right">
                            <div class="btn-group">
                                <button class="btn btn-xs btn-info borxhi-between-js"><span class="glyphicon glyphicon-list"></span></button>
                                <button class="btn btn-xs btn-success borxhi-paguaj-js"><span class="glyphicon glyphicon-ok"></span> <span class="hidden-xs">Paguaj</span></button>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Paguaj?</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Total:</label>

                        <div class="col-xs-9">
                            <div class="onoffswitch4">
                                <input type="checkbox" name="onoffswitch4" class="onoffswitch4-checkbox" id="myonoffswitch4">
                                <label class="onoffswitch4-label" for="myonoffswitch4">
                                    <span class="onoffswitch4-inner"></span>
                                    <span class="onoffswitch4-switch"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group modal-shuma-js" style="display:none">
                        <label class="col-xs-3 control-label">Shuma:</label>

                        <div class="col-xs-9">
                            <input type="text" class="form-control shuma" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Mbyll</button>
                <button type="button" class="btn btn-primary borxhi-paguaj-update-js">Ruaj</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="personaleModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->