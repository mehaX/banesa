<?php
/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 6/25/2016
 * Time: 11:55 PM
 */
?>
<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Produktet</h3>
            </div>

            <table class="table table-striped">
                <thead><tr>
                    <th>Emri</th>
                    <th class="hidden-sm hidden-xs">Çmimi</th>
                    <th class="hidden-sm hidden-xs">Sasia</th>
                    <th class="text-right">Shuma</th>
                </tr></thead>

                <?php foreach ($produktet as $produkti): ?>
                    <tr>
                        <td><?php echo $produkti['emri']; ?></td>
                        <td class="hidden-sm hidden-xs"><?php echo $produkti['qmimi']; ?></td>
                        <td class="hidden-sm hidden-xs"><?php echo $produkti['sasia'] ?></td>
                        <td><div><span class="badge pull-right" role="button" data-toggle="popover" title="Personat" data-content='<?php $this->load->view('produkti-popover', array('produkti' => $produkti, 'borxhet' => $blerja['borxhet'])); ?>'><?php echo number_format($produkti['qmimi']*$produkti['sasia'], 2); ?> €</span></div></td>
                    </tr>
                <?php endforeach; ?>
            </table>

            <div class="panel-footer">
                <strong>Totali</strong>: <span class="badge pull-right"><?php echo number_format(array_sum(array_map(function($val){return $val['qmimi']*$val['sasia'];}, $produktet)), 2); ?> €</span>
            </div>
        </div>
    </div>

    <br class="visible-xs">

    <div class="col-sm-6">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Borxhet:</h3>
            </div>

            <ul class="list-group borxhet-js">
                <?php foreach ($borxhet as $borxhi): ?>
                    <li class="list-group-item <?php if ($borxhi['personi_id'] == $personi_id || $borxhi['paguar'] != null) echo 'list-group-item-success'; ?>" data-personi="<?php echo $borxhi['personi_id']; ?>">
                        <strong><?php echo $borxhi['personi']; ?></strong>
                        <span class="pull-right">
                            <?php if ($personi_id == null || $borxhi['personi_id'] != $personi_id): ?><span class="btn btn-sm glyphicon glyphicon-<?php echo ($borxhi['paguar'] != null ? 'check' : 'unchecked'); ?> borxhi-toggle-js" data-paguar="<?php echo ($borxhi['paguar'] != null ? 1 : 0); ?>"></span><?php endif; ?>
                            <span class="badge <?php if($borxhi['personi_id'] == $user_id): ?>text-warning<?php endif; ?>" role="button" data-toggle="popover" title="Produktet" data-content='<?php $this->load->view('borxhi-popover', array('borxhet' => $blerja['borxhet'], 'personi_id' => $borxhi['personi_id'], 'produktet' => $produktet)); ?>'><?php echo number_format($borxhi['borxh'], 2); ?> €</span>
                        </span>
                    </li>
                <?php endforeach; ?>
            </ul>

            <div class="panel-footer">
                <strong>Borxhi total</strong>:
                <span class="pull-right">
                    <span class="badge"><span class="totali-val-js"><?php echo number_format(array_sum(array_map(function($val) use($personi_id) {return ($val['personi_id'] != $personi_id && $val['paguar'] == null ? $val['borxh'] : 0);}, $borxhet)), 2); ?></span> €</span>
                </span>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        var options = {
            html: true,
            trigger: 'focus hover',
            placement: 'top',
            container: 'body'
        };
        $('[data-toggle="popover"]').each(function(){$(this).popover(options);});
    });
</script>