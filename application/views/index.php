<?php
/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 6/24/2016
 * Time: 6:20 PM
 */
?>

<div class="container">
    <div class="row">
        <div class="col-xs-8">
            <ul class="nav nav-pills">
                <li role="presentation" <?php if ($pill == 1): ?>class="active"<?php endif; ?>><a href="<?php echo base_url(); ?>"><span class="glyphicon glyphicon-asterisk"></span> Aktive</a></li>
                <li role="presentation" <?php if ($pill == 2): ?>class="active"<?php endif; ?>><a href="<?php echo base_url('berllogu'); ?>"><span class="glyphicon glyphicon-trash"></span> Shporta</a></li>
            </ul>
        </div>

        <div class="col-xs-4">
            <div class="pull-right">
                <a href="<?php echo base_url('blerja-add'); ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Shto</a>
            </div>
        </div>
    </div>

    <hr />

    <div class="well">
        <?php foreach ($blerjet as $blerja): ?>
            <div class="blerja-row" data-id="<?php echo $blerja['id']; ?>" data-status="<?php echo $blerja['paguar']; ?>">
                <div class="media">
                    <div class="media-left media-middle">
                        <a role="button"><img class="media-object img-circle" src="<?php echo base_url(array('uploads', 'users', 'thumbnails', $blerja['profile'])); ?>" /></a>
                    </div>

                    <div class="media-body">
                        <h4 class="media-heading"><strong class="<?php if ($blerja['personi_id'] == $user_id) echo 'text-info'; ?>"><?php echo $blerja['personi']; ?></strong></h4>
                        Lista: <strong><?php echo $blerja['lista']; ?></strong><br>
                        <strong><?php echo date('d F Y G:ia', $blerja['koha']); ?></strong><?php if ($blerja['krijuar']): ?>, nga <strong class="<?php if ($blerja['krijuar_id'] == $user_id) echo 'text-info'; ?>"><?php echo $blerja['krijuar']; ?></strong><?php endif; ?>
                    </div>

                    <div class="media-right dropdown-user">
                        <div class="llogarit-blerjen-js">
                            <span class="glyphicon glyphicon-chevron-down"></span>
                        </div>
                    </div>
                </div>

                <div class=" user-infos" style="display:none">
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <div class="btn-group">
                                <a class="btn btn-sm btn-warning" href="<?php echo base_url(array('blerja-edit', $blerja['id'])); ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                                <button class="btn btn-sm btn-danger blerja-toggle-js"><span class="glyphicon glyphicon-<?php echo ($pill == 1 ? 'trash' : 'repeat'); ?>"></span></button>
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="blerja-body-js" style="display:none">

                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
