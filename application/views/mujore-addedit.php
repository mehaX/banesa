<?php
/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 09-Oct-16
 * Time: 5:38 PM
 */
?>
<span class="mujore-js" data-mujore="true"></span>
<div class="personat-template-js hidden">
    <ul class="personi-template-js list-group">
        <?php foreach ($personat as $personi): ?>
            <li class="list-group-item">
                <div class="checkbox">
                    <label>
                        <input data-personi="<?php echo $personi['id']; ?>" type="checkbox" <?php if (isset($produkti['id']) && ka_borxh($produkti['id'], $personi['id'], $borxhet)) echo 'checked'; ?> />
                        <?php echo $personi['personi']; ?>
                    </label>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>

<div class="container">
    <a href="<?php echo base_url('mujoret'); ?>" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span> Pas</a>

    <hr>

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Mujore</h3>
                </div>

                <div class="panel-body">
                    <table class="table produktet-js">
                        <thead><tr>
                            <th>
                                <select class="form-control muaji-js">
                                    <?php for ($i = 1; $i <= 12; $i++): ?>
                                        <option value="<?php echo $i; ?>" <?php if ((isset($blerja) && date('n', $blerja['koha']) == $i) || (!isset($blerja) && $month == $i)) echo 'selected'; ?>><?php echo getMonth(strtotime("2016-$i-01")); ?></option>
                                    <?php endfor; ?>
                                </select>
                            </th>
                            <th>
                                <select class="form-control viti-js">
                                    <?php for ($i = 0; $i < 3; $i++): $y = date('Y', strtotime("-$i years")); ?>
                                        <option value="<?php echo $y ?>" <?php if ((isset($blerja) && date('Y', $blerja['koha']) == $y) || (!isset($blerja) && $year == $y)) echo 'selected'; ?>><?php echo $y ?></option>
                                    <?php endfor ?>
                                </select>
                            </th>
                            <?php foreach ($personat as $personi): ?>
                                <th class="hidden-xs"><?php echo $personi['personi']; ?></th>
                            <?php endforeach; ?>
                            <th></th>
                        </tr></thead>

                        <?php
                        if (isset($blerja['produktet']) && !empty($blerja['produktet'])) foreach ($blerja['produktet'] as $key => $produkti)
                            $this->load->view('blerja-produkti-row', array('is_mujore' => true, 'personat' => $personat, 'produkti' => $produkti, 'borxhet' => $blerja['borxhet'], 'delete' => ($key > 0)));
                        else
                            $this->load->view('blerja-produkti-row', array('is_mujore' => true, 'personat' => $personat, 'delete' => false));
                        ?>
                    </table>

                    <button class="btn btn-sm btn-success pull-right blerja-submit-js"><span class="glyphicon glyphicon-save"></span> Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
