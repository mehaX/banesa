<?php
/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 9/14/2017
 * Time: 9:40 AM
 */
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Between</h3>
    </div>

    <table class="table table-striped betweens">
        <thead><tr>
            <th>Personi</th>
            <th>Data</th>
            <th>Produkti</th>
            <th>Totali</th>
        </tr></thead>

        <?php foreach ($betweens as $between): ?>
            <tr data-personi="<?php echo $between['personi_id']; ?>">
                <td class="<?php if ($between['personi_id'] == $user_id) echo 'text-info'; ?> personale-js" data-id="<?php echo $between['personi_id']; ?>"><?php echo $between['personi']; ?></td>
                <td><?php echo getMonth($between['koha']).date(' Y', $between['koha']) ?></td>
                <td><?php echo $between['emri']; ?></td>
                <td class="text-<?php echo $between['personi_id'] == $user_id ? 'danger' : ''; ?>"><?php echo number_format($between['totali'], 2); ?> €</td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>

