<?php
/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 6/24/2016
 * Time: 6:20 PM
 */
$assets = $this->config->item('assets').'/';
?>
<!DOCTYPE html>
<html>
<head>
    <title>Banesa Pagesat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="<?php echo base_url().$assets; ?>img/icon.png">
    <link rel="stylesheet" href="<?php echo base_url().$assets; ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url().$assets; ?>css/funky.css">
    <link rel="stylesheet" href="<?php echo base_url().$assets; ?>css/toggleswitch.css">
    <link rel="stylesheet" href="<?php echo base_url().$assets; ?>css/chat_popup.css">
    <link rel="stylesheet" href="<?php echo base_url().$assets; ?>css/userlist.css">
    <link rel="stylesheet" href="<?php echo base_url().$assets; ?>css/global.css">

    <?php if ($user_id = $this->session->userdata('user_id')): ?>
        <script>
            var user_id = "<?php echo $this->session->userdata('user_id'); ?>";
            var username = "<?php echo $this->session->userdata('username'); ?>";
            var personi = "<?php echo $this->session->userdata('personi'); ?>";
            var profile = "<?php echo $this->session->userdata('profile'); ?>";
            var auth = "<?php echo sha1("user-".$this->session->userdata('user_id').'-'.$this->session->userdata('personi').'-'.$this->config->item('node_key')); ?>";
        </script>
    <?php endif; ?>

    <script src="<?php echo base_url().$assets; ?>path.js"></script>
    <script src="<?php echo base_url().$assets; ?>js/jquery.min.js"></script>
    <script src="<?php echo base_url().$assets; ?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url().$assets; ?>js/global.js"></script>
</head>

<body>
