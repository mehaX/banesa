<?php
/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 08-Oct-16
 * Time: 4:40 PM
 */
?>
<div class="container">
    <div class="row" style="margin-top:100px;">
        <div class="hidden-xs" style="margin-top:100px;"></div>
    </div>

    <?php $errors = validation_errors(); if (!empty($errors)): ?>
    <div class="alert alert-danger">
        <?php echo $errors; ?>
    </div>
    <?php endif; ?>

    <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Login</h3>
            </div>

            <div class="panel-body">
                <form method="post" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-xs-3 text-right control-label">Emri</label>
                        <div class="col-xs-9">
                            <input type="text" autofocus class="form-control" name="username" value="<?php echo set_value('username'); ?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-3 text-right control-label">Fjalekalimi</label>
                        <div class="col-xs-9">
                            <input type="password" class="form-control" name="password" />
                        </div>
                    </div>

                    <div class="form-group text-center">
                        <input type="submit" class="btn btn-info" value="Login" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
