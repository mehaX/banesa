<div class="row">
    <?php if (empty($produktet)): ?>
        <div class="col-xs-12">
            <h4>Nuk ka</h4>
        </div>
    <?php else: foreach ($produktet as $produkti): ?>
        <?php
        $temp_borxhet = array_filter($borxhet, function($borxhi) use($produkti){
            return $borxhi['produkti_id'] == $produkti['produkti_id'];
        });
        ?>
        <div class="col-sm-6 blerja-row" data-id="<?php echo $produkti['blerja_id'] ?>">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <?php echo $produkti['emri'] ?>
                        <a href="<?php echo base_url(['mujore-edit', $produkti['blerja_id']]) ?>" class="btn btn-xs btn-warning pull-right"><span class="glyphicon glyphicon-pencil"></span></a>
                    </h3>
                </div>

                <ul class="list-group borxhet-js">
                    <?php foreach ($temp_borxhet as $borxhi): ?>
                        <li class="list-group-item <?php if ($borxhi['paguar'] != null) echo 'list-group-item-success'; ?>" data-personi="<?php echo $borxhi['personi_id']; ?>">
                            <strong><?php echo $borxhi['personi']; ?></strong>
                            <span class="pull-right">
                                <span class="btn btn-sm glyphicon glyphicon-<?php echo ($borxhi['paguar'] != null ? 'check' : 'unchecked'); ?> borxhi-toggle-js" data-paguar="<?php echo ($borxhi['paguar'] != null ? 1 : 0); ?>"></span>
                                <span class="badge <?php if($borxhi['personi_id'] == $personi_id): ?>text-warning<?php endif; ?>" role="button" data-toggle="popover" title="Produktet" data-content='<?php $this->load->view('borxhi-popover', array('borxhet' => $blerja['borxhet'], 'personi_id' => $borxhi['personi_id'], 'produktet' => $produktet)); ?>'><?php echo number_format($produkti['qmimi'] / $produkti['total'], 2); ?> €</span>
                            </span>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <div class="panel-footer">
                    <strong>Borxhi total</strong>:
                    <span class="pull-right">
                        <span class="badge"><span class="totali-val-js"><?php echo number_format($produkti['qmimi'], 2); ?></span> €</span>
                    </span>
                </div>
            </div>
        </div>
    <?php endforeach; endif; ?>
</div>