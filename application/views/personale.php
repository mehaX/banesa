<?php
/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 08-May-17
 * Time: 2:24 PM
 */

$pergjithshem = 0.0;
?>
<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">Borxhet Personale</h3>
    </div>

    <table class="table table-striped">
        <thead><tr>
            <th>Personi</th>
            <th>Bleresi</th>
            <th>Borxhi</th>
            <th>Paguar</th>
            <th>Aktuale</th>
            <th></th>
        </tr></thead>

        <?php foreach ($personale as $borxhi): ?>
            <?php
            $other = ($borxhi['id'] == $user_id ? $borxhi['bleresi_id'] : $borxhi['id']);
            $paguar = (!empty($pagesat) ? array_sum(array_map(function($pagesa) use($other, $user_id){
                    if ($pagesa['personi_id'] == $user_id && $pagesa['bleresi_id'] == $other)
                        return doubleval($pagesa['shuma']);
                    else if ($pagesa['personi_id'] == $other && $pagesa['bleresi_id'] == $user_id)
                        return -doubleval($pagesa['shuma']);
                    else
                        return 0.0;
                }, $pagesat)) : 0) * ($borxhi['bleresi_id'] == $user_id ? -1 : 1);
            ?>
            <tr data-personi="<?php echo $borxhi['id']; ?>" data-bleresi="<?php echo $borxhi['bleresi_id'] ?>">
                <td class="<?php if ($borxhi['id'] == $my_id): ?>text-info<?php endif; ?> personale-js" data-id="<?php echo $borxhi['id'] ?>"><?php echo $borxhi['personi']; ?></td>
                <td class="<?php if ($borxhi['bleresi_id'] == $my_id): ?>text-info<?php endif; ?> personale-js" data-id="<?php echo $borxhi['bleresi_id'] ?>"><?php echo $borxhi['bleresi']; ?></td>
                <td><?php echo number_format($borxhi['totali'], 2); ?> €</td>
                <td><span class="paguar" <?php if ($paguar == 0): ?>style="display: none"<?php endif; ?>><span class="value"><?php echo number_format($paguar, 2); ?></span> €</span></td>
                <td class="<?php if ($borxhi['id'] == $user_id) echo 'text-danger'; else if ($borxhi['bleresi_id'] == $user_id) echo 'text-success'; ?>"><?php echo number_format($aktuale = $borxhi['totali'] - $paguar, 2); ?> €</td>
                <td class="text-right">
                    <button class="btn btn-xs btn-info borxhi-between-js"><span class="glyphicon glyphicon-list"></span></button>
                </td>
            </tr>
            <?php
            $pergjithshem += $aktuale * ($borxhi['bleresi_id'] == $user_id ? -1 : 1);
            unset($paguar, $aktuale);
            ?>
        <?php endforeach; ?>

        <?php
        $borxhi_mujor = array_filter($mujore, function($mujore) use($user_id){
            return $mujore['id'] == $user_id;
        });
        $borxhi_mujor = !empty($borxhi_mujor) ? array_shift($borxhi_mujor)['totali'] : 0;

        $paguar_mujore = array_filter($pagesat, function($pagesa) use($user_id){
            return $pagesa['personi_id'] == $user_id && $pagesa['lista_id'] == null;
        });
        $paguar_mujore = !empty($paguar_mujore) ? array_shift($paguar_mujore)['shuma'] : 0;

        $totali_mujor = $borxhi_mujor - $paguar_mujore;
        $pergjithshem += $totali_mujor;

        if ($borxhi_mujor != 0):
            ?>
            <tr data-personi="<?php echo $user_id; ?>">
                <td class="<?php if ($user_id == $my_id) echo 'text-info'; ?> personale-js" data-id="<?php echo $user_id ?>"><?php echo $personi; ?></td>
                <td><strong class="text-danger">Arka</strong></td>
                <td><?php echo number_format($borxhi_mujor, 2); ?> €</td>
                <td><?php if ($paguar_mujore != 0) echo number_format($paguar_mujore, 2).' €'; ?></td>
                <td class="text-danger"><?php echo number_format($totali_mujor, 2); ?> €</td>
                <td class="text-right"><button class="btn btn-xs btn-info borxhi-between-js"><span class="glyphicon glyphicon-list"></span></button></td>
            </tr>
        <?php endif; ?>
    </table>

    <div class="panel-footer">
        <strong class="text-warning">Borxhi i pergjithshem</strong>
        <span class="badge pull-right text-<?php echo ($pergjithshem <= 0 ? 'success' : 'danger'); ?>"><?php echo number_format($pergjithshem, 2) ?> €</span>
    </div>
</div>
