<?php
/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 6/27/2016
 * Time: 5:14 AM
 */
?>

<div class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url(); ?>">Banesa</a>
        </div>

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav">
                <li <?php if ($nav == 1): ?>class="active"<?php endif; ?>><a href="<?php echo base_url(); ?>"><span class="glyphicon glyphicon-th-list"></span> Blerjet</a></li>
                <li <?php if ($nav == 2): ?>class="active"<?php endif; ?>><a href="<?php echo base_url('mujoret'); ?>"><span class="glyphicon glyphicon-retweet"></span> Arka</a></li>
                <li <?php if ($nav == 3): ?>class="active"<?php endif; ?>><a href="<?php echo base_url('borxhet'); ?>"><span class="glyphicon glyphicon-stats"></span> Borxhet</a></li>
            </ul>

            <ul class="nav navbar-nav pull-right">
                <?php if (is_admin()): ?>
                    <li <?php if ($nav == 5): ?>class="active"<?php endif ?>><a href="<?php echo base_url('personat') ?>"><span class="glyphicon glyphicon-user"></span> Personat</a></li>
                <?php endif ?>
                <li>
                    <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <span class="glyphicon glyphicon-user"></span>
                        <?php echo $this->session->userdata('username'); ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" style="left:-50px;">
                        <li><a href="<?php echo base_url(array('new-password', $this->session->userdata('hash'))); ?>"><span class="glyphicon glyphicon-cog"></span> Settings</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?php echo base_url('logout'); ?>"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
