<?php
/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 9/7/2017
 * Time: 11:09 PM
 */
?>
<div class="container">
    <div class="row">
        <div class="col-xs-8">
            <ul class="nav nav-pills">
                <?php for ($i = 0; $i <3; $i++): $year = date('Y', strtotime("-$i years")); ?>
                    <li role="presentation" <?php if ($c_year == $year): ?>class="active"<?php endif ?>><a href="<?php echo base_url("mujoret?year=$year"); ?>"><?php echo $year ?></a></li>
                <?php endfor ?>
            </ul>
        </div>

        <div class="col-xs-4">
            <div class="pull-right">
                <a href="<?php echo base_url('mujore-add'); ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Shto</a>
            </div>
        </div>
    </div>

    <hr />

    <div class="well">
        <?php for ($month = 1; $month <= 12; $month++): ?>
            <div class="blerja-row" data-month="<?php echo $month; ?>" data-year="<?php echo $c_year ?>">
                <div class="media">
                    <div class="media-body">
                        <h4 class="media-heading"><strong><?php echo getMonth($month, true)." $c_year"; ?></strong></h4>
                    </div>

                    <div class="media-right dropdown-user">
                        <div class="llogarit-blerjen-js">
                            <span class="glyphicon glyphicon-chevron-down"></span>
                        </div>
                    </div>
                </div>

                <div class=" user-infos" style="display:none">
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <a class="btn btn-sm btn-success" href="<?php echo base_url(array("mujore-add?year=$c_year&month=$month")); ?>"><span class="glyphicon glyphicon-plus"></span></a>
                        </div>
                    </div>

                    <br>

                    <div class="blerja-body-js" style="display:none">

                    </div>
                </div>
            </div>
        <?php endfor; ?>
    </div>
</div>
