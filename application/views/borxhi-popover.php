<?php
/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 19-Sep-16
 * Time: 8:32 PM
 */
?>
<ul class="list-group">
    <?php foreach ($produktet as $produkti) if (array_search($produkti['id'], array_column(array_filter($borxhet, function($borxhi) use($personi_id){return $borxhi['personi_id'] == $personi_id;}), 'produkti_id')) !== false): ?>
        <li class="list-group-item"><?php echo $produkti['emri']; ?></li>
    <?php endif; ?>
</ul>